/*  Copyright (C) 2020
 *        "Mu Lei" known as "NalaGinrut" <NalaGinrut@gmail.com>
 *  Lambdachip is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or  (at your option) any later version.

 *  Lambdachip is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.

 *  You should have received a copy of the GNU Lesser General Public
 *  License along with this program.
 *  If not, see <http://www.gnu.org/licenses/>.
 */

#include <storage/disk_access.h>
#include <drivers/flash.h>
#include <drivers/gpio.h>
#include <ff.h> // FATFS
#include <fs/fs.h>
#include <kernel.h>
#include <stdio.h>
#include <string.h>
#include <sys/printk.h>
#include <zephyr.h>

#define MAX_PATH_LEN 128

#define LED0_NODE DT_ALIAS(led0)
#if DT_NODE_HAS_STATUS(LED0_NODE, okay)
#define LED0 DT_GPIO_LABEL(LED0_NODE, gpios)
#define LED0_PIN DT_GPIO_PIN(LED0_NODE, gpios)
#define LED0_FLAGS DT_GPIO_FLAGS(LED0_NODE, gpios)
#endif

#define LED1_NODE DT_ALIAS(led1)
#if DT_NODE_HAS_STATUS(LED1_NODE, okay)
#define LED1 DT_GPIO_LABEL(LED1_NODE, gpios)
#define LED1_PIN DT_GPIO_PIN(LED1_NODE, gpios)
#define LED1_FLAGS DT_GPIO_FLAGS(LED1_NODE, gpios)
#endif

#define LED2_NODE DT_ALIAS(led2)
#if DT_NODE_HAS_STATUS(LED2_NODE, okay)
#define LED2 DT_GPIO_LABEL(LED2_NODE, gpios)
#define LED2_PIN DT_GPIO_PIN(LED2_NODE, gpios)
#define LED2_FLAGS DT_GPIO_FLAGS(LED2_NODE, gpios)
#endif

#define LED3_NODE DT_ALIAS(led3)
#if DT_NODE_HAS_STATUS(LED3_NODE, okay)
#define LED3 DT_GPIO_LABEL(LED3_NODE, gpios)
#define LED3_PIN DT_GPIO_PIN(LED3_NODE, gpios)
#define LED3_FLAGS DT_GPIO_FLAGS(LED3_NODE, gpios)
#endif

#define BLEDISABLE_NODE DT_ALIAS(bledisable)
#define BLEDISABLE DT_GPIO_LABEL(BLEDISABLE_NODE, gpios)
#define BLEDISABLE_PIN DT_GPIO_PIN(BLEDISABLE_NODE, gpios)
#define BLEDISABLE_FLAGS DT_GPIO_FLAGS(BLEDISABLE_NODE, gpios)

#define SW0_NODE DT_ALIAS(sw0)
#define SW0 DT_GPIO_LABEL(SW0_NODE, gpios)
#define SW0_PIN DT_GPIO_PIN(SW0_NODE, gpios)
#define SW0_FLAGS DT_GPIO_FLAGS(SW0_NODE, gpios)

const struct device *dev_led_0;
const struct device *dev_led_1;
const struct device *dev_led_2;
const struct device *dev_led_3;

// TODO: erase sector 1 to 7, then write to sector 5
#define FLASH_BLOCK_START_ADDRESS 0x20000

extern char lambdachip_entry[];

struct device *flash_device = NULL;

typedef struct flash_sector {
	uint32_t offset;
	uint32_t size;
} flash_sector;

static FATFS fat_fs;

static struct fs_mount_t mp = {
	.type = FS_FATFS,
	.fs_data = &fat_fs,
};

static const char *disk_mount_pt = "/SD:";

static int lsdir(const char *path);

int mount_disk()
{
	/* raw disk i/o */
	do {
		static const char *disk_pdrv = "SD";
		uint64_t memory_size_mb;
		uint32_t block_count;
		uint32_t block_size;

		if (disk_access_init(disk_pdrv) != 0) {
			printk("ERROR: Storage init ERROR!");
			return -EFAULT;
		}

		if (disk_access_ioctl(disk_pdrv, DISK_IOCTL_GET_SECTOR_COUNT,
				      &block_count)) {
			printk("ERROR: Unable to get sector count; ");
			return -EFAULT;
		}
		printk("Block count %u; ", block_count);

		if (disk_access_ioctl(disk_pdrv, DISK_IOCTL_GET_SECTOR_SIZE,
				      &block_size)) {
			printk("ERROR: Unable to get sector size; ");
			return -EFAULT;
		}
		printk("Sector size %u\n", block_size);

		memory_size_mb = (uint64_t)block_count * block_size;
		printk("Memory Size(MB) %u; ",
		       (uint32_t)(memory_size_mb >> 20));
	} while (0);

	mp.mnt_point = disk_mount_pt;

	int res = fs_mount(&mp);

	if (res == FR_OK) {
		printk("Disk mounted; ");
		return 0;
	} else {
		printk("Error mounting disk; ");
		return -EFAULT;
	}
}

bool fs_exist(struct fs_file_t file, const char *filename)
{
	int rc = fs_open(&file, filename, FS_O_READ);
	if (rc < 0) {
		return false;
	}
	fs_close(&file);
	return true;
}

void upgrade_firmware()
{
	const uint32_t bytes_per_block = 512;
	uint8_t buffer[512] = { 0 };
	struct fs_file_t file;

	// char fname[MAX_PATH_LEN] = {0};

	if (0 == mount_disk()) {
		lsdir(disk_mount_pt);
	} else {
		printk("Disk not mounted, upgrading aborted!\n");
		return;
	}

	// char fname[MAX_PATH_LEN] = "/SD:/FIRMWARE.BIN";
	char fname[MAX_PATH_LEN] = "/SD:/firmware.bin";
	// snprintk
	// snprintk (fname, sizeof (fname), "%s/lambdachip.firmware", "/lfs");
	// snprintk (fname, sizeof (fname), "%s/firmware.bin", "/lfs");
	// static const char *disk_mount_pt = "/SD:";
	// snprintk (fname, sizeof (fname), "%s/firmware.bin", disk_mount_pt);

	// TODO: erase all 1 to 7 sectors

	// Table 4. Flash module organization (STM32F411xC/E) Block Name Block base
	// addresses Size Sector 0 0x0800 0000 - 0x0800 3FFF 16 Kbytes Sector 1 0x0800
	// 4000 - 0x0800 7FFF 16 Kbytes Sector 2 0x0800 8000 - 0x0800 BFFF 16 Kbytes
	// Sector 3 0x0800 C000 - 0x0800 FFFF 16 Kbytes Main memory Sector 4 0x0801
	// 0000 - 0x0801 FFFF 64 Kbytes Sector 5 0x0802 0000 - 0x0803 FFFF 128 Kbytes
	// Sector 6 0x0804 0000 - 0x0805 FFFF 128 Kbytes Sector 7 0x0806 0000 - 0x0807
	// FFFF 128 Kbytes System memory 0x1FFF 0000 - 0x1FFF 77FF 30 Kbytes OTP area
	// 0x1FFF 7800 - 0x1FFF 7A0F 528 bytes Option bytes 0x1FFF C000 - 0x1FFF C00F
	// 16 bytes

	flash_sector sectors[] = { { 0, 0x4000 },	 { 0x4000, 0x4000 },
				   { 0x8000, 0x4000 },	 { 0xC000, 0x4000 },
				   { 0x10000, 0x10000 }, { 0x20000, 0x20000 },
				   { 0x40000, 0x20000 }, { 0x60000, 0x20000 },
				   { 0x80000, 0x20000 } };

	flash_device =
		device_get_binding(DT_CHOSEN_ZEPHYR_FLASH_CONTROLLER_LABEL);
	if (!flash_device) {
		printk("Device not found! device_get_binding failed\n");
		return;
	}

	if (!fs_exist(file, fname)) {
		printk("File doesn't exist, upgrading aborted! filename = %s\n",
		       fname);
		return; // exit, to not erase and write flash
	}

	// clear sector 5~6, 0x8020000~0x805ffff
	for (int i = 5; i < 7; i++) {
		gpio_pin_toggle(dev_led_0, LED0_PIN);
		flash_erase(flash_device, sectors[i].offset, sectors[i].size);
	}

	do {
		int rc = fs_open(&file, fname, FS_O_RDWR);
		if (rc < 0) {
			printk("FAIL: open %s: %d\n", fname, rc);
			break;
		} else {
			printk("Start writing block\n");
			size_t index = 0;
			while (1) {
				size_t read_size;
				read_size = fs_read(&file, &buffer,
						    bytes_per_block);
				// read finish

				// toggle red LED
				gpio_pin_toggle(dev_led_0, LED0_PIN);
				if (read_size >= 0) {
					int ret = flash_write(
						flash_device,
						FLASH_BLOCK_START_ADDRESS +
							index,
						buffer, read_size);
					if (ret == 0) {
						index += read_size;
					}
					printk("Wrote 0x%X bytes\n", index);
				}

				if (read_size < bytes_per_block) {
					// delete file to prevent dead loop
					printk("Exit after writing file to flash\n");
					fs_unlink(fname);
					printk("Delete file %s\n", fname);
					printk("[Firmware Upgrade complete!]\n");
					break;
				}
			}
		}
	} while (0);
}

static int lsdir(const char *path)
{
	int res;
	struct fs_dir_t dirp;
	static struct fs_dirent entry;

	/* Verify fs_opendir() */
	res = fs_opendir(&dirp, path);
	if (res) {
		printk("Error opening dir %s [%d]\n", path, res);
		return res;
	}

	printk("\nListing dir %s ...\n", path);
	for (;;) {
		/* Verify fs_readdir() */
		res = fs_readdir(&dirp, &entry);

		/* entry.name[0] == 0 means end-of-dir */
		if (res || entry.name[0] == 0) {
			break;
		}

		if (entry.type == FS_DIR_ENTRY_DIR) {
			printk("[DIR ] %s\n", entry.name);
		} else {
			printk("[FILE] %s (size = %zu)\n", entry.name,
			       entry.size);
		}
	}

	/* Verify fs_closedir() */
	fs_closedir(&dirp);

	return res;
}

void device_initialize()
{
	int ret;
	//   const struct device* dev_led_0;
	//   const struct device* dev_led_1;
	//   const struct device* dev_led_2;
	//   const struct device* dev_led_3;
	const struct device *dev_4;
	const struct device *dev_5;

	dev_led_0 = device_get_binding(LED0);
	dev_led_1 = device_get_binding(LED1);
	dev_led_2 = device_get_binding(LED2);
	dev_led_3 = device_get_binding(LED3);
	dev_4 = device_get_binding(BLEDISABLE);
	dev_5 = device_get_binding(SW0);

	/* Set LED pin as output */
	ret = gpio_pin_configure(dev_led_0, LED0_PIN,
				 GPIO_OUTPUT_ACTIVE | LED0_FLAGS);
	if (ret < 0) {
		return;
	}
	gpio_pin_set(dev_led_0, LED0_PIN, 0);

	ret = gpio_pin_configure(dev_led_1, LED1_PIN,
				 GPIO_OUTPUT_ACTIVE | LED1_FLAGS);
	if (ret < 0) {
		return;
	}
	gpio_pin_set(dev_led_1, LED1_PIN, 0);

	ret = gpio_pin_configure(dev_led_2, LED2_PIN,
				 GPIO_OUTPUT_ACTIVE | LED2_FLAGS);
	if (ret < 0) {
		return;
	}
	gpio_pin_set(dev_led_2, LED2_PIN, 0);

	ret = gpio_pin_configure(dev_led_3, LED3_PIN,
				 GPIO_OUTPUT_ACTIVE | LED3_FLAGS);
	if (ret < 0) {
		return;
	}
	gpio_pin_set(dev_led_3, LED3_PIN, 0);

	// enable BLE power
	gpio_pin_set(dev_4, BLEDISABLE_PIN, 0);
	ret = gpio_pin_configure(dev_4, BLEDISABLE_PIN,
				 GPIO_OUTPUT_ACTIVE | BLEDISABLE_FLAGS);
	if (ret < 0) {
		return;
	}
	// enable BLE power
	gpio_pin_set(dev_4, BLEDISABLE_PIN, 0);

	// set key as input
	ret = gpio_pin_configure(dev_5, SW0_PIN, GPIO_INPUT | SW0_FLAGS);
	if (ret < 0) {
		return;
	}
}

typedef void (*entry_t)(void);

// TODO: Get the filesize of firmware.bin to determin which sector need to be erase
void main(void)
{
	/* TODO:
	 * 1. Add a REPL shell (include an interpreter)
	 * 2. Load file from storage
	 * 3. Add a special naming convention, if VM detect them then autorun
	 * 4. Add online DEBUG
	 */

	device_initialize();
	k_busy_wait(100000); // 0.1s
	upgrade_firmware();
	// turn on green LED
	gpio_pin_set(dev_led_1, LED1_PIN, 1);
	k_busy_wait(300000); // 0.3s
	// turn off red LED
	gpio_pin_set(dev_led_0, LED0_PIN, 0);
	// turn off green LED
	gpio_pin_set(dev_led_1, LED1_PIN, 0);
	__disable_irq(); // stop OS timer

	// (void)(*)();
	// printk("%d\n", lambdachip_entry);
	// (void)(*lambdachip_entry)();

	// ((entry_t)lambdachip_entry)();

#define APP_ADDRESS (0x8020000)
	// although only when initializing bss segment of the stack, there could be MPU exception.
	// In theory, only when CONFIG_MPU_STACK_GUARD is enabled, arm_core_mpu_disable() is required.
	// But, I choose to disable MPU whenever MPU is enabled here to prevent potential problems.
#if defined(CONFIG_ARM_MPU) || defined(CONFIG_MPU_STACK_GUARD)
	arm_core_mpu_disable();
#endif /* ARM_MPU */
	/* Jump to application */
	// MSP, PSP, SCB->VTOR and MPU will be initialize again in the firmware
	/* Boot the application from APP_ADDRESS */
	uint32_t uid[3] = { 0, 0, 0 };
	memcpy(uid, UID_BASE, sizeof(uid));
	printk("SN=%08X%08X%08X\n", uid[0], uid[1], uid[2]);
	(*(void (**)())(APP_ADDRESS + 4))();
	// }
}
